import argparse
import csv
from apyori import apriori
from sklearn import model_selection
from typing import Tuple

class RecommendationManager:
    def __init__(self,
                 data_file_name: str,
                 min_support: float = 0.0045,
                 min_confidence: float = 0.2,
                 min_lift: float = 1.5,
                 random_state: float = 1,
                 test_size: float = 0.1):
        self.DATA_FILE_NAME = data_file_name
        self.DATA = self.load_data(self.DATA_FILE_NAME)
        self.ITEMS_LIST = self.make_items_list(self.DATA)
        self.TRAINING_DATA, self.TESTING_DATA = self.split_training_and_testing_data(
            self.DATA, random_state, test_size)
        self.RELATIONS = self.find_relations(self.TRAINING_DATA, min_support,
                                             min_confidence, min_lift)
        self.RECOMMENDATION_RULES = self.make_recommendation_rules(self.RELATIONS)

    @staticmethod
    def load_data(data_file_name: str) -> list:
        data = []
        with open(data_file_name) as data_file:
            csv_reader = csv.reader(data_file, skipinitialspace=True)
            for row in csv_reader:
                data.append(row)
        return data

    @staticmethod
    def make_items_list(data: list) -> list:
        items = set()
        for row in data:
            for item in row:
                items.add(item)
        return sorted(list(items))

    @staticmethod
    def split_training_and_testing_data(data: list, random_state: float,
                                        test_size: float) -> Tuple[list, list]:
        training_data, testing_data = model_selection.train_test_split(data, random_state=random_state, test_size=test_size)
        return (training_data, testing_data)

    @staticmethod
    def find_relations(data: list, min_support: float, min_confidence: float,
                       min_lift: float) -> list:
        relations = apriori(data,
                            min_support=min_support,
                            min_confidence=min_confidence,
                            min_lift=min_lift)
        return sorted(list(relations))

    @staticmethod
    def make_recommendation_rules(relations: list) -> list:
        rules = []
        for relation in relations:
            stat = relation.ordered_statistics[0]
            rules.append({
                "items": relation.items,
                "items_base": stat.items_base,
                "items_add": stat.items_add,
                "lift": stat.lift
            })
        return rules

    def get_recommendation_rules_visualization(self) -> str:
        representation_str = 'Association Rules: \n'
        for rule in self.RECOMMENDATION_RULES:
            representation_str += ("Rule: " + str(list(rule['items_base'])) + " --> " + str(list(rule['items_add'])) + "\n")
            representation_str += ("Lift: " + str(rule['lift']) + "\n")
            representation_str += ("========================================\n")
        return representation_str
    
    def get_recommendations(self, items_in_cart: list) -> list:
        recommendations = []
        query_set = frozenset(items_in_cart)
        rules = self.RECOMMENDATION_RULES
        for rule in rules:
            if rule['items_base'] <= query_set: 
                recommendations.append({
                    "item" : list(rule['items_add'])[0],
                    "strength" : rule['lift']
                })
        return sorted(recommendations, key=lambda recommendation: recommendation['strength'], reverse=True)

# if __name__ == '__main__':
#     recommendation_manager = RecommendationManager('store_data.csv')
